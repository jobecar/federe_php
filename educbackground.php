<html>

    <link rel= "stylesheet" type= "text/css" href= "style.css">
    <title>Jobecar Federe's Resume</title>

        <body>
   
                   <div class="topnav">
                       <a href="main.html"> 
                            <img src="home.png" style="width:25px;height:25px;border:0"></a>
                       <a class="active" href="about.html">
                            <img src="about.png" style="width:25px;height:25px;border:0"></a>
                        <a href="portfolio.html">
                            <img src="portfolio.png" style="width:25px;height:25px;border:0"></a>
                    <div class="topnav-right">
                            <input type="text" placeholder="Search..">
                    </div>
                    </div>
                 
                    <div id="content1">
					
                            <table>
                              <tr>
                                  <th>  </th>
                                  <th> School </th>
                                  <th> Address </th>
                                  <th> Started </th>
                                  <th > Ended   </th>
                              </tr >
                               <tr>
                                  <th> Grade School &nbsp &nbsp &nbsp </th>
                                  <td> Bulakanon Central Elementary School </td>
                                  <td> Bulakanon, Makilala, North Cotabato </td>
                                  <td> June 2004 </td>
                                  <td> April 2010 </td>
                               </tr>
                               <tr>
                                  <th> High School &nbsp &nbsp &nbsp </th>
                                  <td> Bulakanon High School </td>
                                  <td> Bulakanon, Makilala, North Cotabato </td>
                                  <td> June 2010 </td>
                                  <td> September 2014 </td>
                               </tr>
                               <tr>
                                  <th> College &nbsp &nbsp &nbsp </th>
                                  <td> University of Southeastern Philippines </td>
                                  <td> Bo. Obrero, Davao City </td> 
                                  <td> June 2014 </td>
                                  <td> <i>(Present)</i> </td>
                               </tr> 
                              
                             </table>
              </div>  
    <footer >
            
            <div id="footer">
                    <p>Find me on social media.</p>
                    <!--
                    <form action="https://www.facebook.com/raceboj.federe" target="_blank">
                    <input id="facebook" type="image" src="facebook.png" width="30" height="30"> </form> 
                    <form action="https://twitter.com/safirra_amanda" target="_blank">
                    <input id="twitter" type="image" src="twitter.png" width="30" height="30"> </form>
                    <form action="https://www.instagram.com/jf.kring/" target="_blank">
                    <input id="instagram" type="image" src="instagram.png" width="30" height="30"> </form>
                    -->
                    <a href="https://www.facebook.com/raceboj.federe" target="_blank"> 
                        <img src="facebook.png" id="facebook" style="width:30px;height:30px;border:0"></a>
                    <a href="https://twitter.com/safirra_amanda" target="_blank">
                        <img src="twitter.png" id="twitter" style="width:30px;height:30px;border:0"></a>
                    <a href="https://www.instagram.com/jf.kring/" target="_blank">
                        <img src="instagram.png" id="instagram" style="width:30px;height:30px;border:0"></a>
                        <br><br>
                    &copy 2018 Jobecar ~
                </div>
    </footer>
</html>